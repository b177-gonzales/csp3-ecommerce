const Product = require("../models/Product");

// Create a new product
/*
	Steps:
	1. Create a conditional statement that will check if the user is an administrator.
	2. Create a new product object using the mongoose model and the information from the request body and the id from the header
	3. Save the new User to the database
*/
module.exports.addProduct = (data) => {

	// User is an admin
	if (data.isAdmin) {

		// Creates a variable "newProduct" and instantiates a new "Product" object using the mongoose model
		// Uses the information from the request body to provide all the necessary information
		let newProduct = new Product({
			name : data.product.name,
			description : data.product.description,
			price : data.product.price
		});

		// Saves the created object to our database
		return newProduct.save().then((product, error) => {

			// Product creation successful
			if (error) {

				return false;

			//  creation failed
			} else {

				return true;

			};

		});

	// User is not an admin
	} else {
		return false;
	};
	

};

// Retrieve all Products
/*
	Steps:
	1. Retrieve all the products from the database
*/
module.exports.getAllProducts = () => {

	return Product.find({}).then(result => {

		return result;

	});

};

// Retrieve all ACTIVE products
/*
	Steps:
	1. Retrieve all the products from the database with the property of "isActive" to true
*/
module.exports.getAllActive = () => {

	return Product.find({isActive : true}).then(result => {
		return result;
	});

};

// Retrieving a specific product
/*
	Steps:
	1. Retrieve the product that matches the product ID provided from the URL
*/
module.exports.getProduct = (reqParams) => {

	return Product.findById(reqParams.productId).then(result => {
		return result;
	});

};

// Update a product
/*
	Steps:
	1. Create a variable "updatedProduct" which will contain the information retrieved from the request body
	2. Find and update the product using the product ID retrieved from the request params property and the variable "updatedProduct" containing the information from the request body
*/
// Information to update a product will be coming from both the URL parameters and the request body
module.exports.updateProduct = (reqParams, reqBody) => {

	// Specify the fields/properties of the document to be updated
	let updatedProduct = {
		name : reqBody.name,
		description	: reqBody.description,
		price : reqBody.price
	};

	// Syntax
		// findByIdAndUpdate(document ID, updatesToBeApplied)
	return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((product, error) => {

		// product not updated
		if (error) {

			return false;

		// product updated successfully
		} else {

			return true;
		};

	});

};

// Archive a product
// In managing databases, it's common practice to soft delete our records and what we would implement in the "delete" operation of our application
// The "soft delete" happens here by simply updating the product "isActive" status into "false" which will no longer be displayed in the frontend application whenever all active products are retrieved
// This allows us access to these records for future use and hides them away from users in our frontend application
// There are instances where hard deleting records is required to maintain the records and clean our databases
// The use of "hard delete" refers to removing records from our database permanently
module.exports.archiveProduct = (reqParams) => {

	let updateActiveField = {
		isActive : false
	};

	return Product.findByIdAndUpdate(reqParams.productId, updateActiveField).then((product, error) => {

		// product not archived
		if (error) {

			return false;

		// product archived successfully
		} else {

			return true;

		}

	});
};

// Capstone 3 archive product
// module.exports.archiveProduct = (reqParams, reqBody) => {

// 	let updateActiveField = {
// 		isActive : reqBody.isActive
// 	};

// 	return Product.findByIdAndUpdate(reqParams.productId, updateActiveField).then((product, error) => {

// 		// product not archived
// 		if (error) {

// 			return false;

// 		// product archived successfully
// 		} else {

// 			return true;

// 		}

// 	});
// };